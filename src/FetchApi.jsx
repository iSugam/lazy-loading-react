import { useState, useRef, useEffect } from "react";
import loader from "./assets/spinner.svg";

const FetchApi = () => {
  const [allData, setAllData] = useState([]); // To store all the data from API
  const [hasMore, setHasMore] = useState(true); // To check if the API has more data
  const [error, setError] = useState(null); // To catch error from API
  const [page, setPage] = useState(1); // For the page count of the API

  const observerTarget = useRef(null);

  // Function for fetching data from API
  const fetchData = async () => {
    setError(null);
    try {
      const res = await fetch(
        `https://openlibrary.org/search.json?q=a&page=${page}`
      );
      const data = await res.json();
      if(data.docs.length == 0) {
        setHasMore(false)
      }

      // Get all the data from the API and set it in the allData state.
      setAllData((prevItems) => [...prevItems, ...data.docs]);
      // To increase the page count every time we need to load more data, as we are calling the fetchData() function inside the IntersectionObserver API, it will increase automatically.
      setPage((prevPage) => prevPage + 1);
    } catch (error) {
      setError(error);
    }
  };

  // Using intersactionObserver for infinite or lazy loading. It will load only after user reaches thebottom of the page
  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting && hasMore) {
          // Calling fetchData() only when entries are intersecting and it contains more data
          fetchData();
        }
      },
      { threshold: 1 }
    );

    if (observer && observerTarget.current) {
      // Observe only when it reaches the bottom of the page or when we see the loading animation.
      observer.observe(observerTarget.current);
    }

    return () => {
      if (observer) {
        // Disconnect or unobserve after observing
        observer.disconnect();
      }
    };
  }, [allData]);

  return (
    <div>
      {/* Mapping through allData to show it in the browser */}
      <div className="all_data">
        {allData.map((data, index) => (
          <div key={`${data.key}${index}`} className="data">
            <h2>{data.title}</h2>
            <p>{data.type}</p>
          </div>
        ))}
      </div>

      {/* If the API has more data, then until it's fetched, show this loading div. */}
      {hasMore && (
        <div className="loading" ref={observerTarget}>
          <img src={loader} alt="spinner gif" width={50} height={50} />
          <h3>Loading...</h3>
        </div>
      )}

      {/* If there's an error, show it here. */}
      {error && <p>Error: {error.message}</p>}
    </div>
  );
};

export default FetchApi;
