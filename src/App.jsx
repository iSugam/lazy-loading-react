import FetchApi from "./FetchApi";

const App = () => {
  return (
    <div className="container">
      <h1>Lazy Loading To Avoid Pagination</h1>
      <h3>
        Using{" "}
        <a
          href="https://openlibrary.org/dev/docs/api/search"
          target="_blank"
          rel="noopener noreferrer"
        >
          openlibrary search API
        </a>
      </h3>
      <FetchApi />
    </div>
  );
}

export default App