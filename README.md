# Section B: React JS Task

## Task2: Lazy Loading To Avoid Pagination

#### Please use npm install to install all the packages.

### Link to Live website

- Lazy loading in react [view Site](https://react-lazy-loading.netlify.app/)

### This is the solution project for infinite scroll or lazy loading assessment

#### The link to the API and the code for fetching
- The API Library that I am using [openlibrary](https://openlibrary.org/search.json?q=a&page=1)
- I already defined the query parameter as "q=a," as you can see in the API URL, which will fetch all the books that have the alphabet "a" included.

- Here is the code for fetching data from the API. 👇

```javascript
const fetchData = async () => {
    setError(null);
    try {
      const res = await fetch(
        `https://openlibrary.org/search.json?q=a&page=${page}`
      );
      const data = await res.json();
      if(data.docs.length == 0) {
        setHasMore(false)
      }
      setAllData((prevItems) => [...prevItems, ...data.docs]);
      setPage((prevPage) => prevPage + 1);
    } catch (error) {
      setError(error);
    }
  };
```

#### Here's the code (comments included) for implementing LazyLoading or Infinite Scroll with the IntersectionObserver API 👇

```javascript
  const observerTarget = useRef(null);
// Using intersactionObserver for infinite or lazy loading. It will load only after user reaches thebottom of the page
useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting && hasMore) {
          fetchData(); // Calling fetchData() only when entries are intersecting and it contains more data
        }
      },
      { threshold: 1 }
    );

    if (observer && observerTarget.current) {
      observer.observe(observerTarget.current); // Observe only when it reaches the bottom of the page or when we see the loading animation.
    }

    return () => {
      if (observer) {
        observer.disconnect(); // Disconnect or unobserve after observing
      }
    };
  }, [allData]);
  ```
